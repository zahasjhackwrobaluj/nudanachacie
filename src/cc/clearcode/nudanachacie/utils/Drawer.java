package cc.clearcode.nudanachacie.utils;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import cc.clearcode.nudanachacie.R;
import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingActivity;
import com.slidingmenu.lib.app.SlidingMapActivity;


public class Drawer {
    public String firstName;
    public String lastName;
    public Bitmap bitmap;
    public SlidingMenu slider;
    public View.OnClickListener buttonClickListener;


    public Drawer(String firstName, String lastName, Bitmap photo, View.OnClickListener buttonClickListener){
        this.firstName = firstName;
        this.lastName = lastName;
        this.bitmap = photo;
        this.buttonClickListener = buttonClickListener;
    }

    public void setupSlider(SlidingActivity activity){
        activity.setBehindContentView(R.layout.drawer);
        this.slider = activity.getSlidingMenu();
        slider.setBehindOffset(80);

        TextView firstName = (TextView) slider.findViewById(R.id.drawer_profile_firstname);
        TextView lastName = (TextView) slider.findViewById(R.id.drawer_profile_lastname);
        ImageView profilePhoto = (ImageView) slider.findViewById(R.id.drawer_profile_photo);

        if ( firstName!=null && this.firstName!=null)
            firstName.setText(this.firstName);

        if ( lastName!=null && this.lastName!=null)
            lastName.setText(this.lastName);

        if ( profilePhoto!=null && this.bitmap!=null)
            profilePhoto.setImageBitmap(bitmap);

        Button events = (Button) slider.findViewById(R.id.drawer_events);
        events.setOnClickListener(buttonClickListener);
        Button movies = (Button) slider.findViewById(R.id.drawer_movies);
        movies.setOnClickListener(buttonClickListener);
        Button logout = (Button) slider.findViewById(R.id.drawer_logout);
        logout.setOnClickListener(buttonClickListener);
    }

    public void setupSlider(SlidingMapActivity activity){
        activity.setBehindContentView(R.layout.drawer);
        this.slider = activity.getSlidingMenu();
        slider.setBehindOffset(80);

        TextView firstName = (TextView) slider.findViewById(R.id.drawer_profile_firstname);
        TextView lastName = (TextView) slider.findViewById(R.id.drawer_profile_lastname);
        ImageView profilePhoto = (ImageView) slider.findViewById(R.id.drawer_profile_photo);

        if ( firstName!=null && this.firstName!=null)
            firstName.setText(this.firstName);

        if ( lastName!=null && this.lastName!=null)
            lastName.setText(this.lastName);

        if ( profilePhoto!=null && this.bitmap!=null)
            profilePhoto.setImageBitmap(bitmap);

        Button events = (Button) slider.findViewById(R.id.drawer_events);
        events.setOnClickListener(buttonClickListener);
        Button movies = (Button) slider.findViewById(R.id.drawer_movies);
        movies.setOnClickListener(buttonClickListener);
        Button logout = (Button) slider.findViewById(R.id.drawer_logout);
        logout.setOnClickListener(buttonClickListener);
    }
}
