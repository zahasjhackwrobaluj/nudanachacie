package cc.clearcode.nudanachacie.utils;

import cc.clearcode.nudanachacie.api.objects.Cinema;
import cc.clearcode.nudanachacie.api.objects.DateShowtime;
import cc.clearcode.nudanachacie.api.objects.responses.JsonGetCinemaShowtimesScreenings;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Robsun
 * Date: 10.11.12
 * Time: 13:21
 * To change this template use File | Settings | File Templates.
 */
public class DataHolder {
    public static Map<Integer, Cinema> cinemas;
    public static List<DateShowtime> showtimes;
    public static Map<String, JsonGetCinemaShowtimesScreenings> screenings;


}
