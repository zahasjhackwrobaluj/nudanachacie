package cc.clearcode.nudanachacie.activities;

import android.os.Bundle;
import android.view.View;
import cc.clearcode.nudanachacie.R;
import cc.clearcode.nudanachacie.utils.Drawer;
import com.slidingmenu.lib.app.SlidingMapActivity;

public class FundamentalMapActivity extends SlidingMapActivity implements View.OnClickListener {

    @Override
    protected boolean isRouteDisplayed() {
        return false;
    }

    private static final String TAG = FundamentalActivity.class.getCanonicalName();
    private static final boolean DEBUG = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);
        Drawer drawer = new Drawer("Jan", "Kowalski", null, this);
        drawer.setupSlider(this);

        /*if (DataHolder.cinemas == null)
            DataHolder.cinemas = new HashMap<Integer, Cinema>();

        if ( DataHolder.showtimes == null )
            DataHolder.showtimes = new ArrayList<DateShowtime>();

        if (DataHolder.screenings == null )
            DataHolder.screenings = new HashMap<String, JsonGetCinemaShowtimesScreenings>();*/

    }

    public int getIdFromResourceUri(String channelUri) {
        return Integer.parseInt(channelUri.substring(13, channelUri.length() - 1));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.drawer_events:
                finish();
                break;

            case R.id.drawer_movies:
                finish();
                break;

            case R.id.drawer_logout:
                finish();
                break;
        }
    }
}