package cc.clearcode.nudanachacie.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import cc.clearcode.nudanachacie.R;
import cc.clearcode.nudanachacie.async.AsyncGetCinemas;
import cc.clearcode.nudanachacie.async.AsyncGetShowtimes;
import cc.clearcode.nudanachacie.facebook.listeners.LoginDialogListener;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 10.11.12
 * Time: 13:12
 * To change this template use File | Settings | File Templates.
 */
public class MainActivity extends FundamentalNonSliderActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getCanonicalName();

    protected ImageButton signInBtn;

    protected AppLoginDialogListener appLoginDialogListener;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        signInBtn = (ImageButton) findViewById(R.id.main_sign_in_btn);
        signInBtn.setOnClickListener(this);
        new AsyncGetCinemas().execute();
        new AsyncGetShowtimes().execute();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (fbConnection.isLogged()) {
            startActivity(new Intent(this, EventsActivity.class));
        }
    }

    @Override
    public void onClick(View view) {

        fbConnection.login(this, fbConnection.permissions, appLoginDialogListener);
    }

    public class AppLoginDialogListener extends LoginDialogListener {

        @Override
        public void onComplete(Bundle values) {
            startActivity(new Intent(MainActivity.this, EventsActivity.class));
        }

        @Override
        public void onCancel() {
            //To change body of implemented methods use File | Settings | File Templates.
        }
    }
}
