package cc.clearcode.nudanachacie.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import cc.clearcode.nudanachacie.R;
import cc.clearcode.nudanachacie.utils.Drawer;

/**
 * Created with IntelliJ IDEA.
 * User: kamil
 * Date: 10.11.12
 * Time: 12:54
 * To change this template use File | Settings | File Templates.
 */
public class MovieActivity extends FundamentalActivity implements View.OnClickListener {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie);
        Drawer drawer = new Drawer("Jan", "Kowalski", null, this);
        drawer.setupSlider(this);
    }

    public int getIdFromResourceUri(String channelUri) {
        return Integer.parseInt(channelUri.substring(13, channelUri.length() - 1));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.drawer_events:
                finish();
                break;

            case R.id.drawer_movies:
                finish();
                break;

            case R.id.drawer_logout:
                Intent intent = new Intent(this, MainActivity.class);
                finish();
                startActivity(intent);
                break;
        }
    }
}
