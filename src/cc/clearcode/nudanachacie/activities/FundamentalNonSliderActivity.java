package cc.clearcode.nudanachacie.activities;

import android.app.Activity;
import android.os.Bundle;
import cc.clearcode.nudanachacie.api.objects.Cinema;
import cc.clearcode.nudanachacie.api.objects.DateShowtime;
import cc.clearcode.nudanachacie.api.objects.responses.JsonGetCinemaShowtimesScreenings;
import cc.clearcode.nudanachacie.facebook.FBConnection;
import cc.clearcode.nudanachacie.utils.DataHolder;
import com.slidingmenu.lib.app.SlidingActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class FundamentalNonSliderActivity extends Activity {
    private static final String TAG = FundamentalNonSliderActivity.class.getCanonicalName();
    private static final boolean DEBUG = true;

    private static final String FB_APP_ID = "248391695290512";
    private static final String FB_APP_SECRET = "3842a2f6396a61124b17ce5b6007b96c";

    protected FBConnection fbConnection;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (DataHolder.cinemas == null)
            DataHolder.cinemas = new HashMap<Integer, Cinema>();

        if (DataHolder.showtimes == null)
            DataHolder.showtimes = new ArrayList<DateShowtime>();

        if (DataHolder.screenings == null)
            DataHolder.screenings = new HashMap<String, JsonGetCinemaShowtimesScreenings>();

        fbConnection = new FBConnection(this);
        fbConnection.initFacebookSession(FB_APP_ID);
    }

    public int getIdFromResourceUri(String channelUri) {
        return Integer.parseInt(channelUri.substring(13, channelUri.length() - 1));
    }

}
