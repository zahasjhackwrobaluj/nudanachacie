package cc.clearcode.nudanachacie.api.objects;

/**
 * Created with IntelliJ IDEA.
 * User: Robsun
 * Date: 10.11.12
 * Time: 09:48
 * To change this template use File | Settings | File Templates.
 */
public class Directors {
   public String permalink;
    public String surname;
    public String image;
    public String posts_uri;
    public String hires_image;
    public String films_played_uir;
    public String resource_uri;
    public String films_directed_uri;
}
