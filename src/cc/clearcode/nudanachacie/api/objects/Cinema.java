package cc.clearcode.nudanachacie.api.objects;

/**
 * Created with IntelliJ IDEA.
 * User: Robsun
 * Date: 09.11.12
 * Time: 21:52
 * To change this template use File | Settings | File Templates.
 */
public class Cinema {
    public String address;
    public int distance;
    public double latitude;
    public double longitude;
    public String name;
    public String resource_uri;
    public String showtimes_uri;
    public String town_uri;
    public int type;
}
