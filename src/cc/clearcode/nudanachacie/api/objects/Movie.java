package cc.clearcode.nudanachacie.api.objects;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Robsun
 * Date: 10.11.12
 * Time: 09:43
 * To change this template use File | Settings | File Templates.
 */
public class Movie {
    public String film_uri;
    public String film_title;
    public List<Screening> screenings;
    public Film film;
}
