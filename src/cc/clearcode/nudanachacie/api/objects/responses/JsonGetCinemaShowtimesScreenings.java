package cc.clearcode.nudanachacie.api.objects.responses;

import cc.clearcode.nudanachacie.api.objects.CinemaMovies;
import cc.clearcode.nudanachacie.api.objects.Paginator;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Robsun
 * Date: 10.11.12
 * Time: 09:52
 * To change this template use File | Settings | File Templates.
 */
public class JsonGetCinemaShowtimesScreenings {
    public Paginator paginator;
    public List<CinemaMovies> objects;
}
