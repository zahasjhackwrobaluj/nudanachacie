package cc.clearcode.nudanachacie.api.objects.responses;

import cc.clearcode.nudanachacie.api.objects.Cinema;
import cc.clearcode.nudanachacie.api.objects.Paginator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Robsun
 * Date: 09.11.12
 * Time: 21:54
 * To change this template use File | Settings | File Templates.
 */
public class JsonGetCinemas {
    public List<Cinema> objects;
    public Paginator paginator;

    public JsonGetCinemas(){
        objects = new ArrayList<Cinema>();
    }
}
