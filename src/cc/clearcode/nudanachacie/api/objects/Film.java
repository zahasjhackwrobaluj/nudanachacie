package cc.clearcode.nudanachacie.api.objects;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Robsun
 * Date: 10.11.12
 * Time: 09:45
 * To change this template use File | Settings | File Templates.
 */
public class Film {
    public String video_uri;
    public String image;
    public int number_of_votes;
    public String checkin_url;
    public String netflix_id;
    public String guess_rating;
    public String title;
    public String related_uri;
    public List<Directors> directors;
    public String title_localized;
    public double average_score;
    public String description;
    public List<String> tags;
    public String posts_uri;
    public String characters_uri;
    public String imdb_code;
    public String hires_image;
    public String short_reviews_uri;
    public List<String> production_country_list;
    public String links_uri;
    public String permalink;
    public String release_year;
    public String release_date;
    public int length;
    public String resource_uri;
}

