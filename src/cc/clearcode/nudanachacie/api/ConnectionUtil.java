package cc.clearcode.nudanachacie.api;

import android.os.Bundle;
import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedOutputStream;import java.io.BufferedReader;import java.io.FileNotFoundException;import java.io.IOException;import java.io.InputStream;import java.io.InputStreamReader;import java.io.UnsupportedEncodingException;import java.lang.String;import java.lang.StringBuilder;import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;


public class ConnectionUtil {
    protected static final String END_LINE = "\r\n";
    private static boolean DEBUG = true;
    private static final int REQUEST_TIMEOUT = 5000;
    private static final String TAG = ConnectionUtil.class.getCanonicalName();

    /**
     * @param connection
     * @param params
     * @throws java.io.IOException
     */
    protected static void addPostParams(HttpURLConnection connection, Bundle params) throws IOException {
        String strBoundary = "3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f";
        Bundle dataParams = new Bundle();

        List<String> list = new ArrayList<String>();

        for (String key : params.keySet()) {
            if (params.getByteArray(key) != null) {
                if(DEBUG) Log.d("ConnectionUtil", "Post File Param: " + key);
                try {
                dataParams.putByteArray(key, params.getByteArray(key));
                } catch (Exception e){
                    ;
                }
                params.remove(key);
                list.add(key);
            }
        }


        for (int i = 0; i < list.size(); i++) {
            params.remove(list.get(i));
        }

        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + strBoundary);
        connection.setReadTimeout(REQUEST_TIMEOUT);
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.connect();

        BufferedOutputStream output = new BufferedOutputStream(connection.getOutputStream());
        if(DEBUG) Log.e(TAG, encodePostBody(params, strBoundary));
        output.write(("--" + strBoundary + END_LINE).getBytes());
        output.write((encodePostBody(params, strBoundary)).getBytes());
        output.write((END_LINE + "--" + strBoundary + END_LINE).getBytes());


        if (!dataParams.isEmpty()) {
            postFiles(output, dataParams, strBoundary);
        }

        output.flush();
    }

    /**
     * @param params
     * @param boundary
     * @return
     */
    public static String encodePostBody(Bundle params, String boundary) {
        if (params == null) {
            return "";
        }

        StringBuilder sb = new StringBuilder();

        for (String key : params.keySet()) {
            sb.append("Content-Disposition: form-data; name=\"" + key + "\"\r\n\r\n" + params.getString(key));
            sb.append("\r\n" + "--" + boundary + "\r\n");
        }

        return sb.toString();
    }

    /**
     * @param parameters
     * @return
     */
    public static String encodeUrl(Bundle parameters) {
        if (parameters == null) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        boolean first = true;

        for (String key : parameters.keySet()) {
            if (first) {
                first = false;
            } else {
                sb.append("&");
            }

            sb.append(URLEncoder.encode(key) + "=" + URLEncoder.encode(parameters.getString(key)));
        }

        return sb.toString();
    }

    /**
     * @param url
     * @param method
     * @param params
     * @return
     * @throws MalformedURLException
     * @throws IOException
     */
    public static String openURL(String url, String method, Bundle params) throws MalformedURLException, IOException {
        if (method.equalsIgnoreCase("GET")) {
            url += encodeUrl(params);
        }

        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        if (!method.equalsIgnoreCase("GET")) {
            addPostParams(connection, params);
        }

        String response = "";

        try {
            response = read(connection.getInputStream());
        } catch (FileNotFoundException e) {
            response = read(connection.getErrorStream());
        }

        if(DEBUG) Log.d(TAG, "Util Response: " + response);

        return response;
    }

    /**
     * @param output
     * @param params
     * @param boundary
     * @throws IOException
     */
    public static void postFiles(BufferedOutputStream output, Bundle params, String boundary) throws IOException {
        for (String key : params.keySet()) {
            output.write(("Content-Disposition: form-data; filename=\"" + key + "\"" + END_LINE).getBytes());
            output.write(("Content-Type: content/unknown" + END_LINE + END_LINE).getBytes());
            output.write(params.getByteArray(key));
            output.write((END_LINE + "--" + boundary + END_LINE).getBytes());
        }
    }

    /**
     * @param inputStream
     * @return
     * @throws IOException
     */
    protected static String read(InputStream inputStream) throws IOException {
        StringBuilder builder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        for (String line = reader.readLine(); line != null; line = reader.readLine()) {
            builder.append(line);
        }

        inputStream.close();
        if(DEBUG) Log.d(TAG, builder.toString());

        return builder.toString();
    }

    public static String requestPOST(String url, Bundle values) {
        String response = "";
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        for (String key : values.keySet()) {
            if (values.getByteArray(key) == null) {
                continue;
            }

            nameValuePairs.add(new BasicNameValuePair(key, values.getString(key)));
        }

        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

            HttpResponse httpResponse = httpClient.execute(httpPost);
            InputStream inputStream = httpResponse.getEntity().getContent();

            response = read(inputStream);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;
    }
}
