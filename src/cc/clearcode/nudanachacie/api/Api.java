package cc.clearcode.nudanachacie.api;

import android.os.Bundle;
import android.util.Log;
import cc.clearcode.nudanachacie.api.objects.responses.JsonGetCinemaShowtimesScreenings;
import cc.clearcode.nudanachacie.api.objects.responses.JsonGetShowtimes;
import cc.clearcode.nudanachacie.api.objects.responses.JsonGetCinemas;
import com.google.gson.Gson;

/**
 * Created with IntelliJ IDEA.
 * User: Robsun
 * Date: 09.11.12
 * Time: 20:32
 * To change this template use File | Settings | File Templates.
 */
public class Api {
    private static final String TAG = Api.class.getCanonicalName();
    private static boolean DEBUG = true;

    private static final Gson gson = new Gson();

    private static final String METHOD = "GET";
    private static final String BASE_URL = "http://api.filmaster.pl";
    private static final String CINEMA_LIST_URL = "/1.1/town/175/cinema/";
    private static final String SHOWTIMES = "/1.1/town/175/cinema/showtimes/"; //then date in format YYYY-MM-DD
    //ex /1.1/town/175/cinema/showtimes/2012-11-09/


    private static String sendRequest(String url) {
        Bundle bundle = new Bundle();
        try {
            String response = ConnectionUtil.openURL(url, METHOD, bundle);
            return response;
        } catch (Exception e) {
            if (DEBUG) Log.e(TAG, "sendJson.error" + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public static JsonGetCinemas getCinemas(int page) {
        if (page == 0) {
            String response = sendRequest(BASE_URL + CINEMA_LIST_URL);
            if (response != null)
                return gson.fromJson(response, JsonGetCinemas.class);
        } else {
            String response = sendRequest(BASE_URL + CINEMA_LIST_URL + "?page=" + page);
            if (response != null)
                return gson.fromJson(response, JsonGetCinemas.class);
        }
        return null;
    }

    public static JsonGetShowtimes getCinemaShowtimes() {
        String response = sendRequest(BASE_URL + SHOWTIMES);
        if (response != null)
            return gson.fromJson(response, JsonGetShowtimes.class);
        return null;
    }

        public static JsonGetCinemaShowtimesScreenings getScreenings(String resourceUri){
        String response = sendRequest(BASE_URL + resourceUri);
        if (response != null)
            return gson.fromJson(response, JsonGetCinemaShowtimesScreenings.class);
        return null;
    }
}

