package cc.clearcode.nudanachacie.facebook.listeners;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;

import com.facebook.android.AsyncFacebookRunner.RequestListener;
import com.facebook.android.FacebookError;

public abstract class UserDataRequestListener implements RequestListener{

	public abstract void onComplete(String response, Object state);

	public void onIOException(IOException e, Object state) {
		// TODO Auto-generated method stub
		e.printStackTrace();
	}

	public void onFileNotFoundException(FileNotFoundException e, Object state) {
		// TODO Auto-generated method stub
		e.printStackTrace();
	}

	public void onMalformedURLException(MalformedURLException e, Object state) {
		// TODO Auto-generated method stub
		e.printStackTrace();
	}

	public void onFacebookError(FacebookError e, Object state) {
		// TODO Auto-generated method stub
		e.printStackTrace();
	}

}
