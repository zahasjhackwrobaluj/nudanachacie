package cc.clearcode.nudanachacie.facebook.listeners;

import android.os.Bundle;

import com.facebook.android.DialogError;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;

public abstract class LoginDialogListener implements DialogListener {

	public abstract void onComplete(Bundle values);

	public void onFacebookError(FacebookError e) {
		// TODO Auto-generated method stub
		
	}

	public void onError(DialogError e) {
		// TODO Auto-generated method stub
		
	}

	public abstract void onCancel();

}
