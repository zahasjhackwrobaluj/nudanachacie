package cc.clearcode.nudanachacie.facebook.objects;

public class Event {

    private String id;
    private String owner;
    private String name;
    private String description;
    private String start_time;
    private String end_time;
    private String location;
    private String picture;

    public static final String idKey = "id";
    public static final String ownerKey = "owner";
    public static final String nameKey = "name";
    public static final String descriptionKey = "description";
    public static final String startTimeKey = "start_time";
    public static final String endTimeKey = "end_time";
    public static final String locationKey = "location";
    public static final String pictureKey = "picture";

    public Event(String id, String owner, String name, String description, String start_time, String end_time, String location, String picture) {
        this.id = id;
        this.owner = owner;
        this.name = name;
        this.description = description;
        this.start_time = start_time;
        this.end_time = end_time;
        this.location = location;
        this.picture = picture;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
