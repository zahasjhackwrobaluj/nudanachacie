package cc.clearcode.nudanachacie.facebook.objects;

public class User {
	
	private String id;
	private String firstName;
	private String lastName;
	private String link;
	private String username;
	private String email;
	private String location;
	private String hometown;
	private String picture;
	private String website;
	private String gender;
	
	public static final String idKey = "id";
	public static final String firstNameKey = "first_name";
	public static final String lastNameKey = "last_name";
	public static final String linkKey = "link";
	public static final String usernameKey = "username";
	public static final String emailKey = "email";
	public static final String locationKey = "location";
	public static final String hometownKey = "hometown";
	public static final String pictureKey = "picture";
	public static final String websiteKey = "website";
	public static final String genderKey = "gender";
	

	public User(String id, String firstName, String lastName,
			String link, String username, String email, String location,
			String hometown, String picture, String website, String gender) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.link = link;
		this.username = username;
		this.email = email;
		this.location = location;
		this.hometown = hometown;
		this.picture = picture;
		this.website = website;
		this.gender = gender;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getHometown() {
		return hometown;
	}

	public void setHometown(String hometown) {
		this.hometown = hometown;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}
	
	
}
