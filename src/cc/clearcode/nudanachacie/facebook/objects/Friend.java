package cc.clearcode.nudanachacie.facebook.objects;

public class Friend {
	
	private String id;
	private String name;
	
	public static final String idKey = "id";
	public static final String nameKey = "name";
	
	public Friend(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
