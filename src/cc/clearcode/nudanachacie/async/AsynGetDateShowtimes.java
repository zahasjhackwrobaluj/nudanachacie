package cc.clearcode.nudanachacie.async;

import android.os.AsyncTask;
import android.util.Log;
import cc.clearcode.nudanachacie.api.Api;
import cc.clearcode.nudanachacie.api.objects.responses.JsonGetCinemaShowtimesScreenings;
import cc.clearcode.nudanachacie.utils.DataHolder;

import java.util.HashMap;

public class AsynGetDateShowtimes extends AsyncTask<Void, Void, Void> {
    private JsonGetCinemaShowtimesScreenings response;
    private String uri;

    public AsynGetDateShowtimes(String uri){
        this.uri = uri;
        if (DataHolder.screenings == null )
            DataHolder.screenings = new HashMap<String, JsonGetCinemaShowtimesScreenings>();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        response = Api.getScreenings(uri);
        DataHolder.screenings.put(uri, response);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
    }
}
