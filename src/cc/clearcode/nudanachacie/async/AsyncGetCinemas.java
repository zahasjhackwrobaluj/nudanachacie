package cc.clearcode.nudanachacie.async;

import android.os.AsyncTask;
import cc.clearcode.nudanachacie.api.Api;
import cc.clearcode.nudanachacie.api.objects.Cinema;
import cc.clearcode.nudanachacie.api.objects.responses.JsonGetCinemas;
import cc.clearcode.nudanachacie.utils.DataHolder;

public class AsyncGetCinemas extends AsyncTask<Void, Void, Void> {
    private int page = 0;
    private JsonGetCinemas response;

    public AsyncGetCinemas() {
        page = 0;
    }

    public AsyncGetCinemas(int page) {
        this.page = page;
    }

    private int getIdFromResourceUri(String channelUri){
        return Integer.parseInt(channelUri.substring(13, channelUri.length() - 1));
    }

    @Override
    protected Void doInBackground(Void... voids) {
        response = Api.getCinemas(page);
        for (Cinema cinema : response.objects) {
            int channel =  getIdFromResourceUri(cinema.resource_uri);// Integer.parseInt(cinema.resource_uri.substring(13, cinema.resource_uri.length() - 1));
            DataHolder.cinemas.put(channel, cinema);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if (response.paginator.next_uri != null) {
            new AsyncGetCinemas(page + 1).execute();
        }
    }
}