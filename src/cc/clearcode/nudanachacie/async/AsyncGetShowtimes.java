package cc.clearcode.nudanachacie.async;


import android.os.AsyncTask;
import cc.clearcode.nudanachacie.api.Api;
import cc.clearcode.nudanachacie.api.objects.DateShowtime;
import cc.clearcode.nudanachacie.api.objects.responses.JsonGetShowtimes;
import cc.clearcode.nudanachacie.utils.DataHolder;

import java.util.ArrayList;

public class AsyncGetShowtimes extends AsyncTask<Void,  Void, Void> {
    private int page = 0;
    private JsonGetShowtimes response;

    public AsyncGetShowtimes() {
        if (DataHolder.showtimes == null)
            DataHolder.showtimes = new ArrayList<DateShowtime>();
        page = 0;
    }

    public AsyncGetShowtimes(int page) {
        this.page = page;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        response = Api.getCinemaShowtimes();
        for (DateShowtime showtime : response.objects){
            DataHolder.showtimes.add(showtime);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        for (DateShowtime showtime : DataHolder.showtimes){
            new AsynGetDateShowtimes(showtime.resource_uri).execute();
        }
    }
}

